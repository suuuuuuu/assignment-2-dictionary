%define NEXT_POINTER 0

%macro colon 2
	%2: 
		dq NEXT_POINTER
		db %1, 0
	%define NEXT_POINTER %2
%endmacro
