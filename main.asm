global _start


section .rodata

key_not_found_msg: 
	db "Key not found", 0
key_too_big_msg: 
	db "The string is too big", 0

section .bss
buffer: resb BUFFER_SIZE

section .text

%define BUFFER_SIZE 256
%define NEXT_POINTER_LENGTH 8

%include "words.inc"

%include "main.inc"

_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	cmp rax, 0
	jne .buffer_not_overflow
		mov rdi, key_too_big_msg
		jmp .print_err_res
	.buffer_not_overflow:	
	mov rdi, rax
	mov rsi, NEXT_POINTER
	push rdx
	call find_word
	pop rdx
	cmp rax, 0 
	jne .key_found
		mov rdi, key_not_found_msg 
		jmp .print_err_res
	.key_found:
	add rax, NEXT_POINTER_LENGTH
	add rax, rdx
	inc rax
	mov rdi, rax
	.print_res:
		call print_string
		call print_newline
		xor rdi, rdi 
		jmp .print_res_end
	.print_err_res:
		call print_error_string
		call print_error_newline
		mov rdi, 1
	.print_res_end:
	call exit
