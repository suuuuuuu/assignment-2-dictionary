extern string_equals

section .text

%define NEXT_POINTER_LENGTH 8
%define true 1

global find_word

; На вход подаются:
;	rdi - указатель на нуль терминированную строку 
;	rsi - указатель на начало словаря
find_word:
	.loop:
		push rdi
		push rsi
		add rsi, NEXT_POINTER_LENGTH ; переходим к значению элемента
		call string_equals ; возвращает в rax
		pop rsi
		pop rdi
		cmp rax, true
		je .found
		mov rsi, [rsi]
		cmp rsi, 0
		jnz .loop
			xor rax, rax ; нужно передать ноль 
		ret
	.found:
		mov rax, rsi
	ret

