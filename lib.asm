section .data

section .text

global exit
global string_length
global print_string_with_code
global print_string
global print_error_string
global print_char_with_code
global print_char
global print_error_char
global print_newline
global print_error_newline
global print_digit
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

TAB_SYMBOL_CODE equ 0x9
SPACE_SYMBOL_CODE equ 0x20
NL_SYMBOL_CODE equ 0xA 
DIGIT_TO_CHAR_SHIFT equ '0'
LINE_END_SYMBOL_CODE equ 0

; Принимает код возврата (rdi) и завершает текущий процесс
exit:
	mov rax, 60
syscall

; Принимает указатель на нуль-терминированную строку (rdi), возвращает её длину (rax)
string_length:
	xor rax, rax ; счетчик длины
	.count_loop:
		cmp byte[rdi + rax], LINE_END_SYMBOL_CODE 
		je .count_loop_end
		inc rax
	jmp .count_loop
	.count_loop_end:
ret

; Принимает указатель на нуль-терминированную строку (rdi), выводит её в stdout
print_string:
	mov rsi, 1
	jmp print_string_with_code

; Принимает указатель на нуль-терминированную строку (rdi), выводит её в stderr 
print_error_string:
	mov rsi, 2
	jmp print_string_with_code

; Принимает указатель на нуль-терминированную строку (rdi), rsi - код потока вывода 
print_string_with_code:
	push rsi ; сохраняем код потока вывода 
	mov rsi, rdi ; указываем на начало строки
	push rsi 
	call string_length
	pop rsi 
	mov rdx, rax ; кол-во символов в строке
	pop rdi ; получаем поток вывода 
	mov rax, 1 ; команда вывести
	syscall
ret

; Принимает код символа (rdi) и выводит его в stdout 
print_char:
	mov rsi, 1
	jmp print_char_with_code

; Принимает код символа (rdi) и выводит его в stderr 
print_error_char:
	mov rsi, 2 
	jmp print_char_with_code

; Принимает код символа (rdi), rsi - код потока вывода 
print_char_with_code:
	mov r9, rsi ; храним код потока
	push rdi
	mov rsi, rsp
	mov rdx, 1
	mov rdi, r9 ; указываем код потока
	mov rax, 1
	syscall
	pop rsi
ret
; Переводит строку (выводит символ с кодом 0xA (roflanZachto)) в stdout
print_newline:
	mov rdi, NL_SYMBOL_CODE
	jmp print_char

; Переводит строку (выводит символ с кодом 0xA (roflanZachto)) в stderr 
print_error_newline:
	mov rdi, NL_SYMBOL_CODE
	jmp print_error_char

; Выводит беззнаковое число 0-9 (rdi). Если оно >= 10, ничего не выводим 
print_digit:
	cmp rdi, 9 
	jle .is_lower
		ret
	.is_lower:
	cmp rdi, 0
	jge .is_greater
		ret
	.is_greater:
	add rdi, DIGIT_TO_CHAR_SHIFT
	jmp print_char


; Выводит беззнаковое 8-байтовое число (rdi) в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r10, 10 ; база десятичного формата
	mov rax, rdi
	xor rcx, rcx 
	.div_loop:
		cmp rax, 0
		jz .div_loop_end 
		mov rdx, 0 ; избегаем ошибки
		div r10 ; rax = результат деления, rdx = остаток
		push rdx
		inc rcx
	jmp .div_loop
	.div_loop_end:
	cmp rcx, 0
	jz print_digit
	.print_digits_loop:
		cmp rcx, 0
		jz .print_digits_loop_end
		pop rdi
		push rcx
		call print_digit
		pop rcx
		dec rcx
	jmp .print_digits_loop
	.print_digits_loop_end:
ret

; Выводит знаковое 8-байтовое число (rdi) в десятичном формате 
print_int:
	cmp rdi, 0
	jge print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки (rdi, rsi), возвращает 1 если они равны, 0 иначе
string_equals:
	.equals_check_loop:
		mov r10b, [rdi] ; r10b - символ из первой строки
		cmp r10b, [rsi]
		jne .not_equals_case
		cmp r10b, LINE_END_SYMBOL_CODE ; конец строк (к этому моменту они равны, в том числе и этим кодом)
		jz .equals_case
		inc rdi
		inc rsi
	jmp .equals_check_loop
	.not_equals_case:
		xor rax, rax 
	ret
	.equals_case:
		mov rax, 1
	ret

; Читает один символ из stdin (rdi) и возвращает его (rax). Возвращает 0 если достигнут конец потока
read_char:
	sub rsp, 2 ; two bytes
	mov rsi, rsp ; куда читать (?) 
	mov rdx, 1 ; кол-во символов прочитать 
	mov rdi, 0 ; поток для вывода
	mov rax, 0 ; команда прочитать 
	syscall ; rax содержит в ответе кол-во прочитанных символов
	cmp rax, 0
	jz .end_of_flow
		pop ax
	ret	
	.end_of_flow:
		add rsp, 2 
	ret 

; Принимает символ (rax), возвращает резульатат (rax, 1 = true, 0 = false)
is_rax_space_symbol:
	cmp rax, SPACE_SYMBOL_CODE ; пробел
	je .if_space
	cmp rax, TAB_SYMBOL_CODE ; табуляция 
	je .if_space
	cmp rax, NL_SYMBOL_CODE ; перевод строки
	je .if_space
		xor rax, rax 
	ret
	.if_space:
		mov rax, 1
	ret

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20 (roflanZachto), табуляция 0x9 (roflanZachto) и перевод строки 0xA (roflanZachto).
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	cmp rsi, 0
	push rdi ; начало буфера (начало буфера)
	push rsi ; размер буфера (размер буфера, начало буфера) 
	.skip_spaces_loop:
		call read_char
		cmp rax, LINE_END_SYMBOL_CODE 
		jnz .spaces_not_empty_input
			pop rsi ; размер буфера (начало буфера)
			pop rdi ; начало буфера ()
			cmp rsi, 1 ; нужен нуль терм.
			jne .spaces_empty_input_not_one_len_buffer
				mov byte[rdi], LINE_END_SYMBOL_CODE ; нуль терм.
				mov rax, rdi
				xor rdx, rdx 
			ret
			.spaces_empty_input_not_one_len_buffer:
				xor rax, rax 
				xor rdx, rdx 
			ret
		.spaces_not_empty_input:
		push rax
		call is_rax_space_symbol
		cmp rax, 0 
		pop rax
		jz .skip_spaces_loop_end
	jmp .skip_spaces_loop
	.skip_spaces_loop_end:
	; has first read symb (2 bytes) on top of the stack 
	pop rsi ; размер буфера (начало буфера)
	pop rdi ; начало буфера ()
	mov rcx, 0 ; счетчик кол-ва считанных
	.reading_word_loop:
		mov byte[rdi + rcx], al ; вставка в буфер чара
		inc rcx ; + счетчик считанных
		push rsi
		push rdi
		push rcx
		call read_char ; rax - чар (или 0 если нет)
		pop rcx ; счетчик кол-ва считанных 
		pop rdi ; начало буфера
		pop rsi ; размер буфера
		cmp rax, LINE_END_SYMBOL_CODE 
		jnz .not_end
			jmp on_word_end
		.not_end:
		push rax
		call is_rax_space_symbol 
		cmp rax, 0
		pop rax
		jz .not_space_symbol
			jmp on_word_end
		.not_space_symbol:
		cmp rcx, rsi
		jne .buffer_size_ok
			xor rax, rax 
		ret
		.buffer_size_ok:
	jmp .reading_word_loop


; rdi - указатель на начало буффера, rcx - кол-во помещенных, rsi - размер буффера
on_word_end:
	mov r10, rcx
	inc r10 ; with 0 term
	cmp r10, rsi
	jle .len_ok
		xor rax, rax 
	ret
	.len_ok:
	mov byte[rdi + rcx], LINE_END_SYMBOL_CODE ; нуль терм.
	mov rax, rdi
	mov rdx, rcx
ret

; Принимает указатель на строку (rdi), пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	mov rcx, 0
	xor rax, rax ; очистим старшие разряды
	.read_digit:
		mov al, [rdi + rcx] ; Прочитать в нижний байт rax следующего байта, предположительно содержащего символ
		sub rax, DIGIT_TO_CHAR_SHIFT ; теперь, если символ - это диджит, его численное выражение должно быть в rax
		cmp rax, 0
		jl .end_of_uint ; диджит должен быть >= 0
		cmp rax, 9 
		jg .end_of_uint ; диджит должен быть <= 9
		; теперь мы уверены, что у нас в руках диджит
		push ax ; поэтому поставим его на вершину стека
		inc rcx ; мы должны считать диджиты
		cmp rcx, 20 ; больше считать в 64 bit ансайнд точно нельзя. Нужно потом проверить на оверфлоу при последнем сложении 
		jle .not_too_long_uint
			.too_long_uint_clear_stack:
				pop rax
				dec rcx
				cmp rcx, 0
				jnz .too_long_uint_clear_stack
			mov rdx, 0
		ret
		.not_too_long_uint:	
	jmp .read_digit
	.end_of_uint:
	cmp rcx, 0
	jnz .not_empty_uint
		xor rdx, rdx 
	ret
	.not_empty_uint:
	mov r9, rcx ; нужно сохранить длину числа
	xor rsi, rsi ; здесь будем хранить результат
	mov r10, 1 ; на что нужно умножить следующий диджит
	mov r11, 10 ; база 
	.make_uint:
		cmp rcx, 0 ; проверим, есть ли еще диджиты
		jz .made_uint ; если не нужно - оканчиваем работу
		xor rax, rax ; очистим старшие разряды 
		pop ax ; получаем разряд
		mul r10 ; получаем его численное представление в числе
		add rsi, rax ; добавляем его к результату
		mov rax, r10 
		jno .not_overflow
			xor rdx, rdx 
		ret
		.not_overflow:
		mul r11 ; подготавливаем "базу" для следующего разряда 
		mov r10, rax
		dec rcx ; нужно уменьшить счетчик оставшихся для обработки разрядов
	jmp .make_uint
	.made_uint:
	mov rax, rsi 
	mov rdx, r9
ret	


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov al, [rdi] ; Прочитать в нижний байт rax следующего байта, предположительно содержащего символ
	cmp al, '-'
	jne .positive
		inc rdi ; курсор - на начало числа
		call parse_uint
		cmp rdx, 0
		jne .negative_not_error
			xor rax, rax 
		ret
		.negative_not_error:
		inc rdx
		neg rax
		jno .negative_not_overflow_on_invert
			xor rax, rax 
			xor rdx, rdx 
		.negative_not_overflow_on_invert:
		ret	
	.positive:
		jmp parse_uint

; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rcx, rcx ; индекс
	.next_char:
		mov r9b, byte [rdi + rcx] ; r9b содержит следующий символ
		cmp r9b, LINE_END_SYMBOL_CODE 
		je .copped_till_null_term
		cmp rcx, rdx
		jl .next_char_not_buffer_overflow
			xor rax, rax 
		ret
		.next_char_not_buffer_overflow:
		mov [rsi + rcx], r9b 
		inc rcx
		jmp .next_char
	.copped_till_null_term:
	mov byte [rsi + rcx], LINE_END_SYMBOL_CODE
	mov rax, rcx
ret
