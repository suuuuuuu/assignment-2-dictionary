.PHONY: clean

EXECNAME=start
ASM=nasm
ASMFLAGS=-f elf64
RMFLAGS=-f
LD=ld

clean:
	rm $(RMFLAGS) *.o $(EXECNAME) 

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

$(EXECNAME): main.o dict.o lib.o
	$(LD) -o $(EXECNAME) $+
